﻿using System;
using NETProjectTutorial.entities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class ReporteFacturaEmpleado : Form
    {
        private DataSet dsSistema;
        private BindingSource bsReporteEmpleados;
        private DataTable tblFacturas;
        public DataSet DsSistema { get => dsSistema; set => dsSistema = value; }
        public DataTable TblFacturas { get => tblFacturas; set => tblFacturas = value; }

        public ReporteFacturaEmpleado()
        {
            InitializeComponent();
            bsReporteEmpleados = new BindingSource();
        }

        private void ReporteFacturaEmpleado_Load(object sender, EventArgs e)
        {
            cmbEmpleadosFactura.DataSource = dsSistema.Tables["Empleado"];
            cmbEmpleadosFactura.DisplayMember = "NA";
            cmbEmpleadosFactura.ValueMember = "Id";

            bsReporteEmpleados.DataSource = dsSistema.Tables["Factura"];
            dgvReporteFacturaEmpleado.DataSource = bsReporteEmpleados;
            dgvReporteFacturaEmpleado.AutoGenerateColumns = true;

            DateText.Text = DateTime.Now.ToString();
        }

        private bool _canUpdate = true;
        private bool _needUpdate = false;
        private void UpdateData()
        {
            if (cmbEmpleadosFactura.Text.Length > 1)
            {

                List<Empleado> searchData = dsSistema.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombre = dataRow.Field<String>("Nombre"),
                        Apellidos = dataRow.Field<String>("Apellidos")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombre.Contains(cmbEmpleadosFactura.Text)));
            }
            else
            {
                RestartTimer();
            }
        }


        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbEmpleadosFactura.Text;

            if (dataSource.Count() > 0)
            {
                cmbEmpleadosFactura.DataSource = dataSource;

                var sText = cmbEmpleadosFactura.Items[0].ToString();
                cmbEmpleadosFactura.SelectionStart = text.Length;
                cmbEmpleadosFactura.SelectionLength = sText.Length - text.Length;
                cmbEmpleadosFactura.DroppedDown = true;
                return;
            }
            else
            {
                cmbEmpleadosFactura.DroppedDown = false;
                cmbEmpleadosFactura.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            _canUpdate = true;
            timer1.Stop();
            UpdateData();

        }

        private void cmbEmpleadosFactura_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private void cmbEmpleadosFactura_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }
        }

        private void cmbEmpleadosFactura_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void BtnVer_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvReporteFacturaEmpleado.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder ver", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;
            FrmReporteFactura frf = new FrmReporteFactura();
            frf.DsSistema = DsSistema;
            frf.DrFactura = drow;
            frf.Show();

            Dispose();
        }
    }
}
