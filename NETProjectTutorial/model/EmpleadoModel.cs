﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> ListEmpleados = new List<Empleado>();
        private implements.DaoImplementsEmpleado daoEmpleado;

        public EmpleadoModel()
        {
            daoEmpleado = new implements.DaoImplementsEmpleado();
        }

        public List<Empleado> GetListEmpleado()
        {
            return daoEmpleado.findAll();
        }

        public void Populate()
        {
            ListEmpleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_data));

            foreach (Empleado e in ListEmpleados)
            {
                daoEmpleado.save(e);
            }
        }

        public void save(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Inss = empleado["INSS"].ToString();
            e.Cedula = empleado["Cédula"].ToString();
            e.Nombre = empleado["Nombre"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Direccion = empleado["Dirección"].ToString();
            e.Tconvencional = empleado["Teléfono"].ToString();
            e.Tcelular = empleado["Celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), empleado["Sexo"].ToString(), true);

            daoEmpleado.save(e);
        }

        public void update(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Inss = empleado["INSS"].ToString();
            e.Cedula = empleado["Cédula"].ToString();
            e.Nombre = empleado["Nombre"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Direccion = empleado["Dirección"].ToString();
            e.Tconvencional = empleado["Teléfono"].ToString();
            e.Tcelular = empleado["Celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), empleado["Sexo"].ToString(), true);

            daoEmpleado.update(e);
        }

        public void delete(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Inss = empleado["INSS"].ToString();
            e.Cedula = empleado["Cédula"].ToString();
            e.Nombre = empleado["Nombre"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Direccion = empleado["Dirección"].ToString();
            e.Tconvencional = empleado["Teléfono"].ToString();
            e.Tcelular = empleado["Celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), empleado["Sexo"].ToString(), true);

            daoEmpleado.delete(e);
        }
    }
}
