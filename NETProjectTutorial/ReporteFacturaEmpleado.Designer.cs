﻿namespace NETProjectTutorial
{
    partial class ReporteFacturaEmpleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbEmpleadosFactura = new System.Windows.Forms.ComboBox();
            this.btnVer = new System.Windows.Forms.Button();
            this.dgvReporteFacturaEmpleado = new System.Windows.Forms.DataGridView();
            this.DateText = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporteFacturaEmpleado)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Empleados";
            // 
            // cmbEmpleadosFactura
            // 
            this.cmbEmpleadosFactura.FormattingEnabled = true;
            this.cmbEmpleadosFactura.Location = new System.Drawing.Point(12, 58);
            this.cmbEmpleadosFactura.Name = "cmbEmpleadosFactura";
            this.cmbEmpleadosFactura.Size = new System.Drawing.Size(259, 21);
            this.cmbEmpleadosFactura.TabIndex = 1;
            // 
            // btnVer
            // 
            this.btnVer.Location = new System.Drawing.Point(286, 58);
            this.btnVer.Name = "btnVer";
            this.btnVer.Size = new System.Drawing.Size(75, 23);
            this.btnVer.TabIndex = 2;
            this.btnVer.Text = "Ver";
            this.btnVer.UseVisualStyleBackColor = true;
            this.btnVer.Click += new System.EventHandler(this.BtnVer_Click);
            // 
            // dgvReporteFacturaEmpleado
            // 
            this.dgvReporteFacturaEmpleado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReporteFacturaEmpleado.Location = new System.Drawing.Point(12, 101);
            this.dgvReporteFacturaEmpleado.Name = "dgvReporteFacturaEmpleado";
            this.dgvReporteFacturaEmpleado.Size = new System.Drawing.Size(491, 255);
            this.dgvReporteFacturaEmpleado.TabIndex = 3;
            // 
            // DateText
            // 
            this.DateText.AutoSize = true;
            this.DateText.Location = new System.Drawing.Point(384, 42);
            this.DateText.Name = "DateText";
            this.DateText.Size = new System.Drawing.Size(37, 13);
            this.DateText.TabIndex = 4;
            this.DateText.Text = "Fecha";
            // 
            // ReporteFacturaEmpleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 372);
            this.Controls.Add(this.DateText);
            this.Controls.Add(this.dgvReporteFacturaEmpleado);
            this.Controls.Add(this.btnVer);
            this.Controls.Add(this.cmbEmpleadosFactura);
            this.Controls.Add(this.label1);
            this.Name = "ReporteFacturaEmpleado";
            this.Text = "Reporte Factura Empleado";
            this.Load += new System.EventHandler(this.ReporteFacturaEmpleado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReporteFacturaEmpleado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbEmpleadosFactura;
        private System.Windows.Forms.Button btnVer;
        private System.Windows.Forms.DataGridView dgvReporteFacturaEmpleado;
        private System.Windows.Forms.Label DateText;
        private System.Windows.Forms.Timer timer1;
    }
}